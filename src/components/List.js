import React, {Fragment} from 'react';
import PokeCard from './PokeCard';

//antd import
import { Row, Divider } from 'antd';


function List({ pokedata }){
    return(
        <Fragment>
            <Divider orientation="left" style={{ fontWeight: 'normal' }}>
                <h2>Lista de Pokemons</h2>
            </Divider>
            <Row justify="center" >
            {pokedata.map((pokemon, index) => {
                let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
                let pokeIndex = pokemon.url.split('/')[pokemon.url.split('/').length - 2]
                    return <PokeCard key={index} to={`/poke-info/${pokeIndex}/${pokemon.name}`} name={pokemon.name} image={`${url}${pokeIndex}.png?raw=true`} />
                })}
            </Row>
        </Fragment>
    );
}

export default List;
import React from 'react';
import { Menu, Typography } from 'antd';
import { Link } from 'react-router-dom';

 
function AppNav(props){ 

const { Title } = Typography;


    return(
        <Menu mode="horizontal">
            <Menu.Item  key="">
                <Link to='/pokemons'>
                    <Title level={4}>PokeApp</Title>
                </Link>
            </Menu.Item>
        </Menu>
    );
}

export default AppNav

import React from 'react';
import PokeCard from './PokeCard';
import { Card, Row, Col, Typography } from 'antd';

const { Title } = Typography;


function PokeDescription({ 
    pokeImage, 
    name, 
    description,
    height,
    weight,
    types,
}){
    return(
        <Row align="middle" >
            <PokeCard image={pokeImage} name={name} />
            <Col span={10}>
            <Card>
                <Title level={4}>
                    Descripcion del pokemon:<br/>
                    {description}
                </Title>
            </Card>
            
            </Col>
            <Col offset={1}>
            <Card style={{ width: 240 }}>
                <Title level={4}>
                    Peso: {weight}                     
                </Title>
                <Title level={4}>
                    Altura: {height}                     
                </Title>
                <Title level={4}>
                    Tipo___________                    
                </Title>
                {types.map((item, idx) => {
                    return(
                        <Title key={idx} level={4}>
                            {item.type.name}                     
                        </Title>
                    )
                })}
            </Card>
            </Col>
        </Row>
    );
}
export default PokeDescription;
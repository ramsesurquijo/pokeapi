import React from 'react';
import {Link} from 'react-router-dom'
import { Card } from 'antd';

const { Meta } = Card;

function PokeCard({name, image, to=''}){
    return(
        <Card
            style={{ width: 350, margin: "2em", }}
            cover={
                <img
                  alt="Pokemon"
                  src={image}
                />
              }
            >
            <Link to={to}>
                <Meta
                    title={name}
                />
            </Link>
            
        </Card>
    );
}
export default PokeCard; 
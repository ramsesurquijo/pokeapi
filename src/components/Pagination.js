import React, { Fragment } from 'react';
import { Button } from 'antd';

const Pagination = ({increment, decrement , page}) =>{

    return(
        <Fragment>
            <Button onClick={decrement}>Previous</Button>
            <Button type="primary">
                {page}
            </Button>
            <Button onClick={increment}>Next</Button>
            
        </Fragment>

    );
} 
export default Pagination;